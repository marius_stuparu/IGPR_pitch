/* Tell JS Hint that yepnope is loaded elsewhere */
/*global yepnope:true */

jQuery(document).ready(function($){
    var setupSlider = function(){
        yepnope([{
            load:[
                'js/vendor/jquery.easing.1.2.js',
                'js/vendor/swfobject.js',
                'js/vendor/jquery.anythingslider.min.js',
                'js/vendor/jquery.anythingslider.video.min.js',
                'js/vendor/jquery.anythingslider.fx.min.js'
            ],
            complete: function(){
                $('#slider').anythingSlider({
                    expand:         true,
                    buildStartStop: false,
                    autoPlay:       true,
                    delay:          5000,
                    hashTags:       false
                })
            }
        }])
    }

    var setupSideMenu = function(){
        var $side = $('#side'),
            $main = $('#main'),
            sideHeight = $side.outerHeight(true),
            mainHeight = $main.outerHeight(true)

        if( sideHeight < mainHeight ) {
            $side.outerHeight(mainHeight)
        } else {
            $main.outerHeight(sideHeight)
        }
    }
    
    // All functions defined, now determine how they load
    var Init = function(){
        if( $('#slider').length ) { // if there's a #slider with content, set it up
            setupSlider()
        }

        if( $('#side.submenu').length ) { // if the .submenu is present and has content, set it up
            setupSideMenu()
        }
    }
    
    // Everything set up - run Init()
    Init()
})